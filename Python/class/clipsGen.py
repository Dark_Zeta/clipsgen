class ClipsGen:

    def __init__(self):
        self.templateList = []
        self.factList = []
        self.ruleList = []

    def createTemplate(self, name, list):

        tempString = '(deftemplate ' + name

        for i in list:
            tempString += ' (slot ' + i + ')'

        tempString += ')\n'

        return(tempString)

    def createFact(self, template, list):

        tempString = '(' + template

        for i in list:
            tempString += ' (' + i[0] + ' "' + i[1] + '")'

        tempString += ')\n'

        return(tempString)

    def createRule(self, name, conditionList, resultList):

        tempString = '(defrule ' + name + "\n"

        for i in conditionList:
            tempString += i

        tempString += '\n=>\n'

        for i in resultList:
            tempString += i

        tempString += ")\n"

        return(tempString)

    def createComparison(self, comparison, conditions):
        # starts comparison
        tempString = "(" + comparison + "\n"

        # builds for each condition
        for condition in conditions:
            # checks if it's a nested condition
            # if not, run as usual
            if(condition[0] != "and" and condition[0] != "or"):
                tempString += "(" + condition[0]

                # iteratates through sublist in second position
                for sub in condition[1]:
                    tempString += " (" + sub[0] + " ?" + sub[1] + ")"

                tempString += ")\n"

            # else
            # recursively build nested condition
            else:
                # first list item --> comparison
                # second list item --> conditions list
                tempString += ClipsGen.createComparison(self,condition[0], condition[1]) + "\n"

        # ends comparison
        tempString += ")"

        #print("WIP")
        return(tempString)

# For future works
    # # template format --> [template, [[slot1, actor], [slot2, actor]]]
    # def createAssignment(self, name, template):
    #     tempString = "?" + name + " <- " + "(" + template[0]
    #
    #     for sub in template[1]:
    #         tempString += " (" + sub[0] + " ?" + sub[1] + ")"
    #
    #     tempString += ")\n"
    #
    #     return tempString

    def createIf(self, eq, list):

        tempString = '(if ('

        if eq == True:
            tempString += 'eq'
        else:
            tempString += 'neq'

        for i in list:
            tempString += ' ?' + i

        tempString += ')\nthen\n'

        return tempString

    def createAssertion(self, definition):

        tempString = '(assert ' + definition[0]

        for sub in definition[1]:
            tempString += " (" + sub[0] + " ?" + sub[1] + ")"

        tempString += ')\n'

        return(tempString)
