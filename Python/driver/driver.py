from clipsGen import ClipsGen

def templateMenu(cg, currentTemplates):

    while(1):

        # prints basic menu
        print("0. Exit")
        print("1. Build Template")
        print("2. View Templates")
        print("3. Remove Templates")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            break

        # build templates
        elif(choice == "1"):
            buildTemplate(cg, currentTemplates)

        # view templates
        elif(choice == "2"):
            if(len(cg.templateList) == 0):
                print("Error. You must add templates first!\n")

            else:
                viewTemplates(cg)

        # remove templates
        elif(choice == "3"):
            if(len(cg.templateList) == 0):
                print("Error. You must add templates first!\n")

            else:
                viewTemplates(cg)
                removeTemplates(cg, currentTemplates)

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

def buildTemplate(cg, currentTemplates):
    name = input("What would you like to name your template? ")
    print()

    if (name in currentTemplates[0]):
        print("Error. That template already exists.\n")

    else:
        slotNames = []

        while(1):
            try:
                slotCount = int(input("How many slots should the template have? "))
                print()

                for i in range(slotCount):
                    slot = input("What is the name of slot " + str(i + 1) + ": ")

                    slotNames.append(slot)

                break

            except:
                print("Invalid input. Please enter a whole number.")
                print("Please try again.\n")

        template = cg.createTemplate(name, slotNames)

        print("\nThis is your created template:")
        print(template)

        accept = input("Would you like to accept the template? (Y/n) ").lower()
        print()

        while(accept != "" and accept != "y" and accept != "n"):
            print("Invalid choice! Try again.")
            accept = input("Would you like to accept the template? (Y/n) ").lower()
            print()

        if(accept == "" or accept == "y"):
            print("Adding!\n")
            cg.templateList.append(template)

            if(name not in currentTemplates[0]):
                currentTemplates[0].append(name)
                currentTemplates[1].append(slotNames)

        else:
            print("Template rejected.\n")

def viewTemplates(cg):
    print("Templates: ")

    count = 1

    for item in cg.templateList:
        print("Template " + str(count) + ":", item, end="")

        count += 1

    print()

def removeTemplates(cg, currentTemplates):
    while(1):
        try:
            print("To exit, enter -1")
            item = int(input("Which item would you like to remove? (enter the number associated) "))

            if(item > len(cg.templateList) or item < -1):
                print("Invalid option! Try again.\n")

            elif(item == -1):
                break

            else:
                del cg.templateList[item - 1]

                del currentTemplates[0][item - 1]
                del currentTemplates[1][item - 1]

                print("Template removed.\n")
                break

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def getTemplate(currentTemplates):
    while(1):
        try:
            item = int(input("Which template would you like to use? (enter the number associated) "))

            if(item > len(currentTemplates[0]) or item < -1):
                print("Invalid option! Try again.\n")

            else:
                return [currentTemplates[0][item - 1], currentTemplates[1][item - 1]]

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def rulesMenu(cg, currentTemplates):

    while(1):

        # prints basic menu
        print("0. Exit")
        print("1. Build Rule")
        print("2. View Rules")
        print("3. Remove Rule")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            break

        # build templates
        elif(choice == "1"):
            newRulesMenu(cg, currentTemplates)

        # view templates
        elif(choice == "2"):
            if(len(cg.ruleList) == 0):
                print("You must add some first!")

            else:
                viewRule(cg)

        # remove templates
        elif(choice == "3"):
            if(len(cg.ruleList) == 0):
                print("You must add some first!")

            else:
                viewRule(cg)
                removeRule(cg)

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

def viewRule(cg):
    print("Rules: ")

    count = 1

    for item in cg.ruleList:
        print("Rule " + str(count) + ":", item, "\n")

        count += 1

    print()

def removeRule(cg):
    while(1):
        try:
            print("To exit, enter -1")
            item = int(input("Which rule would you like to remove? (enter the number associated) "))

            if(item > len(cg.ruleList) or item < -1):
                print("Invalid option! Try again.\n")

            elif(item == -1):
                break

            else:
                del cg.ruleList[item - 1]
                print("Rule removed.")
                break

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def newRulesMenu(cg, currentTemplates):

    actorList = []
    conditionList = []

    # first sublist --> ifs
    # second sublist --> assertions
    resultList = [[], []]

    while(1):

        # prints basic menu
        print("0. Exit")
        print("1. Actors")
        print("2. Conditions")
        print("3. Results")
        print("4. Submit")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            break

        # go to actor menu
        elif(choice == "1"):
            actorMenu(actorList)

        # go to condition menu
        elif(choice == "2"):
            if(len(actorList) == 0):
                print("Error. You must add actors first.\n")

            else:
                conditionMenu(cg, currentTemplates, actorList, conditionList)

        # go to result menu
        elif(choice == "3"):
            if(len(actorList) == 0):
                print("Error. You must add actors first.\n")

            else:
                resultMenu(cg, currentTemplates, actorList, resultList)

        # submit new rule
        elif(choice == "4"):
            # createRule
            if(len(conditionList) == 0 or len(resultList[1]) == 0):
                print("Error! You need to add conditions and results (assertions at minimum) first.")

            else:
                name = input("What would you like to name your rule? ")
                print()

                viewConditions(conditionList)
                condition = [getCondition(conditionList)]
                print()

                results = []

                answer = input("Do you need to use an if-result? (Y/n) ").lower()
                print()

                while(answer != "" and answer != "y" and answer != "n"):
                    print("Error. Invalid option. Please try again.")

                    answer = input("Do you need another result? (Y/n) ").lower()
                    print()

                if(answer == "y" or answer == ""):
                    viewResultsIfs(resultList)
                    results.append(getResultIf(resultList))
                    print()

                    viewResultsAsserts(resultList)
                    results.append(getResultAssert(resultList) + ")")
                    print()

                else:
                    viewResultsAsserts(resultList)
                    results.append(getResultAssert(resultList))
                    print()


                print("Creating rule ...")

                cg.ruleList.append(cg.createRule(name, condition, results))

                print()

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

def actorMenu(currentActors):

    while(1):

        # prints basic menu
        print("0. Exit")
        print("1. Add Actors")
        print("2. View Actors")
        print("3. Remove Actors")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            break

        # build actors
        elif(choice == "1"):
            addActors(currentActors)

        # view actors
        elif(choice == "2"):
            if(len(currentActors) == 0):
                print("Error. You must add actors first.\n")

            else:
                viewActors(currentActors)

        # remove actors
        elif(choice == "3"):
            if(len(currentActors) == 0):
                print("Error. You must add actors first.\n")

            else:
                viewActors(currentActors)
                removeActors(currentActors)

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

def addActors(currentActors):
    actorName = input("What would you like to name this actor? ")

    currentActors.append(actorName)
    print()

def viewActors(currentActors):
    print("Actors: ")

    count = 1

    for item in currentActors:
        print("Actor " + str(count) + ":", item)

        count += 1

    print()

def removeActors(currentActors):
    while(1):
        try:
            print("To exit, enter -1")
            item = int(input("Which item would you like to remove? (enter the number associated) "))

            if(item > len(currentActors) or item < -1):
                print("Invalid option! Try again.\n")

            elif(item == -1):
                break

            else:
                del currentActors[item - 1]
                print("Actor removed.\n")
                break

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def getActor(currentActors):
    while(1):
        try:
            item = int(input("Which actor would you like to use? (enter the number associated) "))

            if(item > len(currentActors) or item < -1):
                print("Invalid option! Try again.\n")

            else:
                return(currentActors[item - 1])

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def conditionMenu(cg, currentTemplates, currentActors, currentConditions):

    while(1):

        # prints basic menu
        print("0. Exit")
        print("1. Add Condition")
        print("2. View Conditions")
        print("3. Remove Condition")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            break

        # add conditions
        elif(choice == "1"):
            addCondition(cg, currentTemplates, currentActors, currentConditions)

        # view conditions
        elif(choice == "2"):
            if(len(currentConditions) == 0):
                print("Error. You must add some conditions first.\n")

            else:
                viewConditions(currentConditions)

        # remove conditions
        elif(choice == "3"):
            if(len(currentConditions) == 0):
                print("Error. You must add some conditions first.\n")

            else:
                viewConditions(currentConditions)
                removeConditions(currentConditions)

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

def addCondition(cg, currentTemplates, currentActors, currentConditions):

    while(1):

        # prints basic menu
        print("0. Exit")
        print("1. Add Comparison")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            break

        # build actors
        elif(choice == "1"):
            comparitor = input("Are you using 'and' or 'or'? ")
            currentConditions.append(cg.createComparison(comparitor, buildComparison(cg, currentTemplates, currentActors)))
            print()

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

def viewConditions(currentConditions):
    print("Conditions: ")

    count = 1

    for item in currentConditions:
        print("Condition " + str(count) + ":", item)

        count += 1

    print()

def removeConditions(currentConditions):
    while(1):
        try:
            print("To exit, enter -1")
            item = int(input("Which item would you like to remove? (enter the number associated) "))

            if(item > len(currentConditions) or item < -1):
                print("Invalid option! Try again.\n")

            elif(item == -1):
                break

            else:
                del currentConditions[item - 1]
                print("Condition removed.")
                break

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def getCondition(currentConditions):
    while(1):
        try:
            item = int(input("Which conditions would you like to use? (enter the number associated) "))

            if(item > len(currentConditions) or item < -1):
                print("Invalid option! Try again.\n")

            else:
                return(currentConditions[item - 1])

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

# builds a rule condition for comparisons
def buildComparisonCondition(cg, templates, actors):
    temp = []

    viewTemplates(cg)
    choice = getTemplate(templates)

    template = choice[0]

    slotNum = len(choice[1])

    tempDef = []

    print()

    for i in range(slotNum):
        slot = []

        slotCheck = choice[1][i]

        print("Slot:", slotCheck)
        print()

        viewActors(actors)
        actor = getActor(actors)
        print()

        slot.append(slotCheck)
        slot.append(actor)

        tempDef.append(slot)

    temp = [template, tempDef]

    return temp

def buildComparison(cg, templates, actors):
    conditions = []

    while(1):
        try:
            num = int(input("How many items are you comparing? "))
            break

        except:
            print("That's not a number! Try again.")

    print()

    for i in range(num):
        answer = input("Is this a nested comparison? (y/n) ").lower()
        print()

        while(answer != "n" and answer!= "y"):
            print("That's not a valid option! Try again.")
            answer = input("Is this a nested comparison? (y/n) ").lower()
            print()

        if(answer == "n"):
            conditions.append(buildComparisonCondition(cg, templates, actors))

        else:
            conditions.append(buildNestedComparison(cg, templates, actors))

    return conditions

def buildNestedComparison(cg, templates, actors):
    print("\n***START NESTING***\n")
    comparitor = input("Are you using 'and' or 'or'? ")

    conditions = []
    nesting = []

    while(1):
        try:
            num = int(input("How many items are you comparing? "))
            break

        except:
            print("That's not a number! Try again.")

    print()

    for i in range(num):
        answer = input("Is this a nested comparison? (y/n) ").lower()

        while(answer != "n" and answer!= "y"):
            print("That's not a valid option! Try again.")
            answer = input("Is this a nested comparison? (y/n) ").lower()

        if(answer == "n"):
            nesting.append(buildComparisonCondition(cg, templates, actors))

        else:
            nesting.append(buildNestedComparison(cg, templates, actors))

    conditions = [comparitor, nesting]

    print("\n***END NESTING***\n")

    return conditions

def resultMenu(cg, currentTemplates, currentActors, currentResults):

    while(1):

        # prints basic menu
        print("0. Exit")
        print("1. Add Result")
        print("2. View Results")
        print("3. Remove Result (If)")
        print("4. Remove Result (Assertions)")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            break

        # add result
        elif(choice == "1"):
            addResult(cg, currentTemplates, currentActors, currentResults)

        # view results
        elif(choice == "2"):
            if(len(currentResults[0]) == 0 and len(currentResults[1]) == 0):
                print("Error. You must add results first.\n")

            else:
                viewResultsIfs(currentResults)
                print()
                viewResultsAsserts(currentResults)
                print()

        # remove results - if
        elif(choice == "3"):
            if(len(currentResults[0]) == 0):
                print("Error. You must add results first.\n")

            else:
                viewResultsIfs(currentResults)
                removeResultsIf(currentResults)

        # remove results - assertions
        elif(choice == "3"):
            if(len(currentResults[1]) == 0):
                print("Error. You must add results first.\n")

            else:
                viewResultsAsserts(currentResults)
                removeResultsAsserts(currentResults)

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

def addResult(cg, currentTemplates, currentActors, currentResults):

    while(1):

        # prints basic menu
        print("0. Exit")
        print("1. Add If")
        print("2. Add Assertion")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            break

        # build if
        elif(choice == "1"):
            eqCheck = input("Are you using testing for equality (eq) or inequality (neq)? ")
            print()

            while(eqCheck != "eq" and eqCheck != "neq"):
                print("\nSorry, that's not a valid answer. Please use 'eq' or 'neq'.")
                eqCheck = input("Are you using testing for equality (eq) or inequality (neq)? ")
                print()

            if (eqCheck == "eq"):
                equality = True
            else:
                equality = False

            eqActors = []

            viewActors(currentActors)

            for i in range(2):
                eqActors.append(getActor(currentActors))

            currentResults[0].append(cg.createIf(equality, eqActors))
            print()

        # build assertion
        elif(choice == "2"):
            print("Assert:\n")

            assertion = buildComparisonCondition(cg, currentTemplates, currentActors)

            currentResults[1].append(cg.createAssertion(assertion))

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

def viewResultsIfs(currentResults):
    print("Results (Ifs): ")

    count = 1

    for item in currentResults[0]:
        print("If " + str(count) + ":", item)

        count += 1

    print()

def viewResultsAsserts(currentResults):
    print("Results (Assertions): ")

    count = 1

    for item in currentResults[1]:
        print("Assertion " + str(count) + ":", item)

        count += 1

    print()

def getResultIf(currentResults):
    while(1):
        try:
            item = int(input("Which if would you like to use? (enter the number associated) "))

            if(item > len(currentResults[0]) or item < -1):
                print("Invalid option! Try again.\n")

            else:
                return(currentResults[0][item - 1])

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def getResultAssert(currentResults):
    while(1):
        try:
            item = int(input("Which assertion would you like to use? (enter the number associated) "))

            if(item > len(currentResults[1]) or item < -1):
                print("Invalid option! Try again.\n")

            else:
                return(currentResults[1][item - 1])

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def removeResultsIf(currentResults):
    while(1):
        try:
            print("To exit, enter -1")
            item = int(input("Which if would you like to remove? (enter the number associated) "))

            if(item > len(currentResults[0]) or item < -1):
                print("Invalid option! Try again.\n")

            elif(item == -1):
                break

            else:
                del currentResults[0][item - 1]
                print("Result removed.")
                break

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def removeResultsAsserts(currentResults):
    while(1):
        try:
            print("To exit, enter -1")
            item = int(input("Which assertion would you like to remove? (enter the number associated) "))

            if(item > len(currentResults[1]) or item < -1):
                print("Invalid option! Try again.\n")

            elif(item == -1):
                break

            else:
                del currentResults[1][item - 1]
                print("Result removed.")
                break

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

def factsMenu(cg, templates):
    while(1):
        print("0. Exit")
        print("1. Add Facts")
        print("2. View Facts")
        print("3. Remove Facts")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            break

        # add fact
        elif(choice == "1"):
            fact = addFact(cg, templates)
            cg.factList.append(cg.createFact(fact[0], fact[1]))

        # view facts
        elif(choice == "2"):
            if(len(cg.factList) == 0):
                print("Error. You must add facts first.\n")

            else:
                print()
                viewFacts(cg)

        # remove facts
        elif(choice == "3"):
            if(len(cg.factList) == 0):
                print("Error. You must add facts first.\n")

            else:
                print()
                viewFacts(cg)
                removeFacts()

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

def addFact(cg, templates):
    temp = []

    viewTemplates(cg)
    choice = getTemplate(templates)

    template = choice[0]

    slotNum = len(choice[1])

    tempDef = []

    print()

    for i in range(slotNum):
        slot = []

        slotCheck = choice[1][i]

        print("Slot:", slotCheck)
        print()

        actor = input("Who do you want to fill this role? ")
        print()

        slot.append(slotCheck)
        slot.append(actor)

        tempDef.append(slot)

    temp = [template, tempDef]

    return temp

def viewFacts(cg):
    print("Facts: ")

    count = 1

    for item in cg.factList:
        print("Fact " + str(count) + ":", item, end="")

        count += 1

    print()

def removeFacts():
    while(1):
        try:
            print("To exit, enter -1")
            item = int(input("Which fact would you like to remove? (enter the number associated) "))

            if(item > len(cg.factList) or item < -1):
                print("Invalid option! Try again.\n")

            elif(item == -1):
                break

            else:
                del cg.factList[item - 1]
                print("Fact removed.")
                break

        except:
            print("You entered a value that isn't a whole number! Try again.\n")

#==============================================================================#
def mainMenu(cg, currentTemplates):

    while(1):

        # prints basic menu
        print("0. Exit")
        print("1. Output CLIPS to file")
        print("2. Output facts to file")
        print("3. Templates")
        print("4. Rules")
        print("5. Facts")
        choice = input("Which would you like to do? ")
        print()

        # exit
        if(choice == "0"):
            print("Goodbye!")
            break

        # file writing
        elif(choice == "1"):
            print("This is where you'll port everything you've done to a CLIPS file!")
            print("WARNING: Any file you enter will be overwritten. If you don't want this to happen, merely enter a new file name and we'll create it for you!")
            print()

            filename = input("What file would you like to write to? (no extensions!) ")
            filename = filename + ".clp"
            print()

            # gets fact file
            factsFile = input("What file would you like to use as the facts (INCLUDE extensions)? ")

            outfile = open(filename, "w")

            # writing Templates
            print("\nWriting templates ...")
            outfile.writelines(cg.templateList)

            # writing Rules
            print("Writing rules ...")
            outfile.writelines(cg.ruleList)

            # writing end material
            print("Writing end material ...")
            outfile.write("(reset)\n")
            outfile.write('(load-facts) "' + factsFile + '")\n')
            outfile.write("(run)\n")
            outfile.write("(facts)\n")
            outfile.write("(exit)\n")


            print("\nDone!\n")
            outfile.close()

        # facts output
        elif(choice == "2"):
            print("This is where you'll port all the facts you've done to a text file!")
            print("WARNING: Any file you enter will be overwritten. If you don't want this to happen, merely enter a new file name and we'll create it for you!")
            print()

            filename = input("What file would you like to write to? (no extension!) ")
            filename = filename + ".txt"
            print()

            outfile = open(filename, "w")

            outfile.writelines(cg.factList)
            print("Writing facts ...")

            print("\nDone!\n")
            outfile.close()

        # go to template menu
        elif(choice == "3"):
            templateMenu(cg, currentTemplates)

        # create rules
        elif(choice == "4"):
            # only allows adding if the info has templates
            if(len(cg.templateList) == 0):
                print("Error. You must add templates before creating rules.\n")

            else:
                rulesMenu(cg, currentTemplates)

        # create facts
        elif(choice == "5"):
            if(len(currentTemplates[0]) == 0):
                print("Error. You must add templates first.\n")

            else:
                factsMenu(cg, currentTemplates)

        # invalid option
        else:
            print("Error. You've chosen an invalid option.")
            print("Please try again.\n")

cg = ClipsGen()

def main():
    print("Welcome to the CLIPS creator!\n")

    cg = ClipsGen()

    allTemplates = [[], []]

    mainMenu(cg, allTemplates)


main()
