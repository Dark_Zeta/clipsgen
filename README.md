# Clipsgen

## About Clipsgen

Clipsgen is an open source framework that aims to alleviate the need for manually creating CLIPS files. It provides the foundation for .clp generation through the use of high level languages such as Python, C++, and C#.

Clipsgen is covered under the the MIT Permissive License, so it can be freely redistributed and used however the individual sees fit.

## Installation

There is no need to install Clipsgen to you system as it is a standalone framework (that is not targeted at a specific language or architecture).

Simply clone it or download from Gitlab and add the framework to you project and include the Clipsgen class.

We do suggest actually installing the language you intend to use and the CLIPS language to interpret the generated .clp files though.

## Usage

Documentation for each supported language can be find in the Docs folder of that language.

### Screenshots
